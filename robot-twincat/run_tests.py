#from ADSConn import *

#netId = [192, 168, 56, 1, 1, 1]
#port = 350
#keyword = "avantHILObject.ModelParameters.set_seatSwitch"

#pyWriteByVarName(netId, port, keyword, True)
#pyReadByVarName(netId, port, keyword)

from ADSConn import *

netId = [192, 168, 56, 1, 1, 1]
port = 851
keyword = "MAIN.TEST"

def testReadWrite(newValue):
    pyWriteByVarName(netId, port, keyword, newValue)
    a = pyReadByVarName(netId, port, keyword)
    print("Python:\tValue given to C++ function:\t", newValue)
    print("Python:\tValue returned to python:\t", a)
    print("Python:\tCompare values: ", newValue == a, "\n")

testReadWrite(1)
testReadWrite(0.5)
testReadWrite(1.1)
testReadWrite(0.1)
testReadWrite(0.3333333333333333)
