from ADSConn import *

class TestLibrary(object):

    netId = [192, 168, 56, 1, 1, 1]
    port = 350

    states = {
        "ON": True,
        "OFF": False
    }

    def __init__(self):
        print("Address:", self.netId, "Port:", self.port)

    def _keyword_should_be(self, keyword, expected):
        # Validate function arg
        expected_PLC_value = self.states.get(expected)

        actual_PLC_value = pyReadByVarName(self.netId, self.port, keyword)
        print("PYTHON:\tTry to read keyword: ", keyword);
        if actual_PLC_value != expected_PLC_value:
            raise AssertionError('%s != %s' % (actual_PLC_value, expected_PLC_value))

    def _set_keyword(self, keyword, newValue):
        # Validate function arg
        new_PLC_value = self.states.get(newValue)
        print("PYTHON:\tTry to set", keyword, "to", newValue)
        pyWriteByVarName(self.netId, self.port, keyword, new_PLC_value)

    def seat_switch_should_be(self, expected):
        self._keyword_should_be("MAIN.SEAT_SWITCH", expected)

    def parking_brake_should_be(self, expected):
        self._keyword_should_be("MAIN.PARKING_BRAKE", expected)

    def set_seat_switch(self, newValue):
        self._set_keyword("MAIN.SEAT_SWITCH", newValue)

    def set_parking_brake(self, newValue):
        self._set_keyword("MAIN.PARKING_BRAKE", newValue)
