# Notes

## Requirements
* Windows operating system
* Python 3.7 (32-bit)
* Robot Framework

## How?
	1. Install python 3.7 32-bit https://www.python.org/ftp/python/3.7.4/python-3.7.4.exe
	2. Install robot framework
		* python -m pip robotframework
	3. Set the netId and port from the TestLibrary.py to wanted AMSAddress and port
	4. Run "robot -d output tests.robot" in this directory

## In case of errors
* ADS return codes: https://infosys.beckhoff.com/english.php?content=../content/1033/tc3_adscommon/html/ads_returncodes.htm&id=3486737418493835572