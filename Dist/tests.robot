*** Settings ***
Documentation     	Example tests using ADS -connectivity
Library           	TestLibrary.py


*** Test Cases ***
If Seat Switch is OFF, Parking Brake should be ON to prevent the vehicle movement
	[Setup]								Driver Seated With No Brakes
	Set Seat Switch						OFF
	Parking Brake Should Be				ON
	[Teardown]							Vehicle Parked


*** Keywords ***
Driver Seated With No Brakes
	[Documentation]						Default test setup
	Set Parking Brake					OFF
	Set Seat Switch						ON

Vehicle Parked
	[Documentation]						Default test tear down
	Set Parking Brake					ON
	Set Seat Switch						OFF