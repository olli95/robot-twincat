# TwinCat notes

![twincat connection components](./Doc/TwinCATConn.jpg)

## Testing Library

* Developed as one Visual Studio solution with two projects (python and c++ code)

* Python 3.7 (32-bit)
 	* Robot Framework
		* Creating test libraries *http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#creating-test-libraries*

* C++
	* Used for adding ADS Connectivity to Python as extension.
	* Preparations:
		* Add the dll file from *C:\TwinCAT\AdsApi\TcAdsDll\TcAdsDll.dll* to *C:\Windows\System32*
	* Dependencies:
		* C:\TwinCAT\AdsApi\TcAdsDll\Lib\TcAdsDll.lib
	* Imports:
		* #include "C:\TwinCAT\AdsApi\TcAdsDll\Include\TcAdsDef.h"
		* #include "C:\TwinCAT\AdsApi\TcAdsDll\Include\TcAdsApi.h"
	* More info:
		* Beckhoff ADS Documentation: https://infosys.beckhoff.com/content/1033/tc3_adscommon/html/tcadscommon_introads.htm?id=6479552931026062113
		* Python Extension Patterns: https://pythonextensionpatterns.readthedocs.io/en/latest/refcount.html#
	
## TwinCat XAE Shell
* Windows OS (only that twincat supports)
* TwinCat version 3
	
	* Create project:
	    1. create new twincat XAE shell -project
	    2. inside the project solution explorer, right-click PLC > Add new item > create standard plc project
	    3. lib references: Tc2_Standard, Tc2_System, Tc3_Module
	    4. Code to: POUs > MAIN(PRG)
	* Run project:
	    1. "Activate Configuration" button
	    2. Login
	    3. Start
### PLC program used for the tests:
```
PROGRAM MAIN
VAR
	SEAT_SWITCH			: BOOL;
	PARKING_BRAKE		: BOOL;
END_VAR

IF (SEAT_SWITCH = FALSE) THEN
	PARKING_BRAKE := TRUE;
ELSE
	PARKING_BRAKE := FALSE;
END_IF
```