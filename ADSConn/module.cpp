#include <Python.h>
#include "adsFunctions.h"
#include <iostream>
#include <string>

/**
* Parse the ads netId from arguments in python list type
*/
static AmsNetId parseNetId(PyObject* adsNetId)
{
	AmsNetId netId;
	Py_ssize_t listSize = PyList_Size(adsNetId);

	if (listSize != 6) throw std::invalid_argument("Invalid ADS Net Id");

	for (int i = 0; i < 6; i++)
	{
		PyObject* pItem = PyList_GetItem(adsNetId, i);
		netId.b[i] = (unsigned char)PyLong_AsLong(pItem);
	}

	return netId;
}

static PyObject* pyReadByVarName(PyObject*, PyObject* args)
{
	assert(!PyErr_Occurred());
	assert(args);
	Py_INCREF(args);

	PyObject*	adsNetId;
	int			adsPort;
	char*		varName;

	int nErr = PyArg_ParseTuple(args,	// arguments as PyObject
		"O!is",							// Info about arguments (O! = Python object subtype, i = int, s = string)
		&PyList_Type, &adsNetId,		// Type for python object subtype and pointer for storing it
		&adsPort,						// ADS port as int
		&varName);						// variable name as string
	if (!nErr) std::cout << "Error" << std::endl;

	// Parse address for ADS
	AmsNetId netId;
	try
	{
		netId = parseNetId(adsNetId);
	}
	catch (std::invalid_argument& e)
	{
		PyErr_SetString(PyExc_ValueError, "Invalid ADS Net Id");
		return NULL;
	}

	// Connect to ADS
	ADSConn ads = ADSConn(netId, adsPort);
	double retValue;

	try
	{
		retValue = ads.readByVarName<double>(varName);
		std::cout << "C:\tVariable " << varName << " value is " << retValue << std::endl;
	}
	catch (long e)
	{
		std::cout << "ERR " << e << std::endl;
		PyErr_SetString(PyExc_ValueError, "Invalid ADS Net Id");
		return NULL;
	}

	// Build python format return value
	PyObject * pyFloatTypeRetVal = Py_BuildValue("d", retValue);

	// Free args reference
	Py_DECREF(args);
	return pyFloatTypeRetVal;
}

static PyObject* pyWriteByVarName(PyObject*, PyObject* args)
{
	assert(!PyErr_Occurred());
	assert(args);
	Py_INCREF(args);

	// Arguments
	PyObject*	adsNetId;
	int			adsPort;
	char*		varName;
	double		valueToWrite;

	int nErr = PyArg_ParseTuple(args,	// arguments as PyObject
		"O!isd",						// Info about arguments (O! = Python object subtype, i = int, s = string, f = float) More: https://docs.python.org/3/c-api/arg.html
		&PyList_Type, &adsNetId,		// Type for python object subtype and pointer for storing it
		&adsPort,						// port as int
		&varName,						// variable name as string
		&valueToWrite);					// value to write as float

	if (!nErr)
	{
		PyErr_SetString(PyExc_ValueError, "Error parsing arguments");
		return NULL;
	}

	// Parse address for ADS
	AmsNetId netId;
	try
	{
		netId = parseNetId(adsNetId);
	}
	catch (std::invalid_argument& e)
	{
		PyErr_SetString(PyExc_ValueError, "Invalid ADS Net Id");
		return NULL;
	}

	// Create ads instance
	ADSConn ads = ADSConn(netId, adsPort);

	// Try to write value
	try
	{
		ads.writeByVarName<double>(varName, valueToWrite);
		double currentValue = ads.readByVarName<double>(varName);

		std::cout << "C:\tSet "
			<< varName
			<< " to "
			<< valueToWrite
			<< ". Current value is "
			<< currentValue
			<< std::endl;
	}
	catch (long e)
	{
		std::cout << "ADS Error code " << e << std::endl;
		PyErr_SetString(PyExc_ValueError, "ADS Error");
		return NULL;
	}

	// Free args reference
	Py_DECREF(args);				
	Py_RETURN_NONE;
}

static PyMethodDef ADSConn_methods[] = {
	// The first property is the name exposed to Python, fast_tanh, the second is the C++
	// function name that contains the implementation.

	{ "pyReadByVarName", (PyCFunction)pyReadByVarName, METH_VARARGS, nullptr },
	{ "pyWriteByVarName", (PyCFunction)pyWriteByVarName, METH_VARARGS, nullptr },

	// Terminate the array with an object containing nulls.
	{ nullptr, nullptr, 0, nullptr }
};

static PyModuleDef ADSConn_module = {
	PyModuleDef_HEAD_INIT,
	"ADSConn",										 // Module name to use with Python import statements
	"Provides ads client functionality for python",  // Module description
	0,
	ADSConn_methods									 // Structure that defines the methods of the module
};

PyMODINIT_FUNC PyInit_ADSConn() {
	return PyModule_Create(&ADSConn_module);
}