#pragma once

#include <iostream>
#include <windows.h>

#include "C:\TwinCAT\AdsApi\TcAdsDll\Include\TcAdsDef.h"
#include "C:\TwinCAT\AdsApi\TcAdsDll\Include\TcAdsApi.h"

/**
* Get size of an array dynamically
*/
unsigned int getSize(char arr[])
{
	int i = 0;
	while (arr[i] != '\0') i++;
	return i + 1; // +1 For the \0 character
}

class ADSConn
{
private:
	AmsNetId amsId;
	unsigned int port;

public:
	ADSConn(AmsNetId amsId, unsigned int port)
	{
		this->amsId = amsId;
		this->port = port;
	}

	/**
	* Reads the current boolean value from variable with specified name in twincat and returns it.
	*
	* @param varName	The name of the PLC variable to read
	* @return			The boolean value of the read variable
	*/
	template <typename T>
	T readByVarName(char varName[])
	{
		long      nErr, nPort;
		AmsAddr   Addr;
		PAmsAddr  pAddr = &Addr;
		ULONG     lHdlVar;
		T		  nData;

		// cout << "Read variable: " << varName << " Size: " << getSize(varName) << endl;

		// Open communication port on the ADS router
		nPort = AdsPortOpen();
		nErr = AdsGetLocalAddress(pAddr);
		if (nErr) throw nErr; // cerr << "Error: AdsGetLocalAddress: " << nErr << '\n';

		// Address to remote PLC
		pAddr->netId = this->amsId;

		// Port for remote PLC
		pAddr->port = this->port;

		// Get the handle of the PLC-variable <szVar> 
		nErr = AdsSyncReadWriteReq(pAddr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(lHdlVar), &lHdlVar, strlen(varName), varName);
		if (nErr) throw nErr; // cerr << "Error: AdsSyncReadWriteReq: " << nErr << '\n';

		// Read the value of a PLC-variable, via handle 
		nErr = AdsSyncReadReq(pAddr, ADSIGRP_SYM_VALBYHND, lHdlVar, sizeof(nData), &nData);
		if (nErr) throw nErr; // cerr << "Error: AdsSyncReadReq: " << nErr << '\n';

		// Release the handle of the PLC-variable
		nErr = AdsSyncWriteReq(pAddr, ADSIGRP_SYM_RELEASEHND, 0, sizeof(lHdlVar), &lHdlVar);
		if (nErr) throw nErr; // cerr << "Error: AdsSyncWriteReq: " << nErr << '\n';

		// Close the communication port
		nErr = AdsPortClose();
		if (nErr) throw nErr; // cerr << "Error: AdsPortClose: " << nErr << '\n';

		return nData;
	}

	/**
	* Writes given value to specified PLC variable.
	*
	* @param varName	The name of the PLC variable to write
	* @param newValue	The value to be written to PLC variable
	* @return			The boolean value of the read variable
	*/
	template <typename T>
	void writeByVarName(char varName[], T newValue)
	{
		long      nErr, nPort;
		AmsAddr   Addr;
		PAmsAddr  pAddr = &Addr;
		ULONG     lHdlVar;
		T		  nData;

		// Open communication port on the ADS router
		nPort = AdsPortOpen();

		nErr = AdsGetLocalAddress(pAddr);
		if (nErr) throw nErr; // cerr << "Error: AdsGetLocalAddress: " << nErr << '\n';

		// Address to remote PLC
		pAddr->netId = this->amsId;

		// Port for remote PLC
		pAddr->port = this->port;

		// Get the handle of the PLC-variable <szVar> 
		nErr = AdsSyncReadWriteReq(pAddr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(lHdlVar), &lHdlVar, getSize(varName), varName);
		if (nErr) throw(nErr); // cerr << "Error: AdsSyncReadWriteReq: " << nErr << '\n';

		// Set the value of the PLC variable to new value
		nData = newValue;
		nErr = AdsSyncWriteReq(pAddr, ADSIGRP_SYM_VALBYHND, lHdlVar, sizeof(nData), &nData);
		if (nErr) throw nErr; // cerr << "Error: AdsSyncWriteReq: " << nErr << '\n';

		// Release the handle of the PLC-variable
		nErr = AdsSyncWriteReq(pAddr, ADSIGRP_SYM_RELEASEHND, 0, sizeof(lHdlVar), &lHdlVar);
		if (nErr) throw nErr; // cerr << "Error: AdsSyncWriteReq: " << nErr << '\n';

		// Close the communication port
		nErr = AdsPortClose();
		if (nErr) throw nErr; // cerr << "Error: AdsPortClose: " << nErr << '\n';
	}
};